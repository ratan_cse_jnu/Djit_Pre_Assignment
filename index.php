<?php
include_once '/vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;
?>

<html>
    <head>
        <title>ABridge Corp.</title>
    </head>
    <style type='text/css'>
        .bgtitle {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 170px;
            width:970px;
            line-height:1.8;}

        .footer {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 0px;
            width:970px;
            line-height:1.8;}

        .search {                       
            font-size:12px;
            padding:10px;
            margin-left: 770px;
            line-height:0.0;}        
        </style>
        <div style="text-align:center">
        <div class="bgtitle"><center><h2><font color="red">ABridge Corp.</font></h2></center>
            <a href="Views/Djit/Djit2017/Login/signup.php">Registration</a>   <a href="Views/Djit/Djit2017/Login/login.php">Login</a></div>        
        <div class="search">
            <form method="post" action="search.php">
                <input type="text" placeholder="Search..." name="query" />
                <input type="submit" value="Find" name="completedsearch" />
            </form>
        </div>
        <center><h3>List of Threads</h3></center>
    </div>

<?php
$obj = new Thread();
$data = $obj->index();
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
    <body>
    <center>         
        <table border="1" width="1000">
            <tr>
                <th width="70">SL</th>
                <th width="480">Thread</th>
                <th width="300">Poster</th>
                <th width="150">Published Date</th>
            </tr>
            <?php
            $serial = 1;
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>

                    <tr>
                        <td><center> <?php echo $serial++ ?></center></td>
                    <td><center> <?php echo $value['thread_desc'] ?></center><br/>
                    <b>Comment:</b><?php echo $value['comment'] ?></td>
                    <td><center> <img src="<?php echo 'Views/Djit/Djit2017/Thread/upload/' . $value['image']; ?>" alt='Invalid File' class='thumbnail' height='200px;' width='250px;' /> </center></td> 
                    <td><center> <?php echo $value['time'] ?></center></td>    
                    </tr>

                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3">
                        No Available Thread. <a href="Views/Djit/Djit2017/Login/index.php">Add Thread</a>
                    </td>
                </tr>
            <?php } ?>

        </table>
        <br/>
        <div class="footer"><center><h2><font color="black">&copy; Copyright 2017 ABridge Corp.</font></h2></center></div>        
    </body>
</html>
