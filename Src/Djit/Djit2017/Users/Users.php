<?php
namespace Apps\Djit\Djit2017\Users;

use PDO;

class Users
{
    public $id = '';
    public $fullname = '';    
    public $dbuser = 'root';
    public $dbpass = '';
    public $username = '';
    public $password = '';
    public $password2 = '';
    public $email = '';
    public $conn = '';
    public $data = '';
    public $userexist = '';
    public $role = '';
    public $image = '';


    public function __construct()
    {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->conn = new PDO('mysql:host=localhost;dbname=abridge', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '')
    {
        if (!empty($data['uname'])) {
            $this->username = $data['uname'];
        }
        if (!empty($data['pw1'])) {
            $this->password = $data['pw1'];
        }
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['fullname'])) {
            $this->fullname = $data['fullname'];
        }
        if (!empty($data['role'])) {
            $this->role = $data['role'];
        }
        if (!empty($data['image'])) {
            $this->image = $data['image'];
        }
        return $this;
    }

    public function index()
    {
        try {
            $query = "SELECT * FROM `users` ";
            $q = $this->conn->query($query) or die('Unable to query');
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show()
    {

    }

    public function signup()
    {
        try {
            $usrname = "'$this->username'";
            $qr = "SELECT * FROM users WHERE username=" . $usrname;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user = $STH->fetch(PDO::FETCH_ASSOC);

            $email = "'$this->email'";
            $qr = "SELECT * FROM users WHERE email=" . $email;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user2 = $STH->fetch(PDO::FETCH_ASSOC);
            if (!empty($user)) {
                $_SESSION['Message'] = "Username already exists";
                header('location:signup.php');
            } elseif (!empty($user2)) {
                $_SESSION['Message'] = "Already register with this email";
                header('location:signup.php');
            } else {
                $verification_code = uniqid();
                $query = "INSERT INTO users(id,unique_id,username,password,email,is_active, is_admin,is_delete,created)
                VALUES(:id, :uid,:un, :pw, :email, :ia, :iad,:isd,:cr)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':uid' => uniqid(),
                    ':un' => $this->username,
                    ':pw' => $this->password,
                    ':email' => $this->email,
                    ':ia' => 0,
                    ':iad' => $this->role,
                    ':isd' => 0,
                    ':cr' => date("Y-m-d g:i"),
                ));
                $_SESSION['success_msg1'] = "<h4><font color='red'><center>Successfully signup. Login for continue</center></font></h4>";

                header('location:login.php');
        }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function login()
    {
        $usrname = "'$this->username'";
        $password = "'$this->password'";
        $qr = "select * from users where username =  $usrname && password = $password";
        //$qr = "select * from `users` where `username`='$usrname' AND `password`='" . md5($password) . "';";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);
        if (isset($user) && !empty($user)) {
            if ($user['is_active'] == 0) {
                $_SESSION['Message'] = "<h3><font color='red'><center>Your account not verified yet. Check your email and verify</center></font></h3>";
                header('location:login.php');
            } else {
                $_SESSION['user'] = $user;
                header('location:index.php');
            }
        } else {
            $_SESSION['Message'] = "<h3><font color='red'><center>Invalid Username or Password</center></font></h3>";
            header('location:login.php');
        }
    }

    public function viewall()
    {
        $qry="SELECT * FROM users WHERE is_delete='0'";
        $stmt=  $this->conn->query($qry);
        $stmt->execute();
        $table=$stmt->fetchAll();
        return $table;
    }

      public function view()
    {
        $qry="SELECT * FROM users WHERE username='$this->username'";
        $stmt=  $this->conn->query($qry);
        $stmt->execute();
        $table=$stmt->fetchAll();
        return $table;
    }

    public function Last_login(){
        $sql="UPDATE `users` SET `last_login`= now()  WHERE username = '$this->username'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }
    
    public function noOfPosts(){

        $qr = "select * from thread where thread_creator = '$this->username' ";
        $STH = $this->conn->query($qr);
        echo  $STH->rowCount();
        
    }    

}
