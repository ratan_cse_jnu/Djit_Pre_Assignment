<?php

namespace Apps\Djit\Djit2017\Thread;

use PDO;

class Thread {

    public $dbuser = 'root';
    public $dbpass = '';
    public $username = '';
    public $conn = '';
    public $data = '';
    public $userexist = '';
    public $role = '';
    public $id = '';
    public $thread_desc = '';
    public $comment = '';
    public $like = '';
    public $image = '';

    public function __construct() {
        date_default_timezone_set("Asia/Dhaka");
        $this->conn = new PDO('mysql:host=localhost;dbname=abridge', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '') {
        if (array_key_exists('thread_desc', $data)) {
            $this->thread_desc = $data['thread_desc'];
        }
        if (array_key_exists('comment', $data)) {
            $this->comment = $data['comment'];
        }        
        if (array_key_exists('like', $data)) {
            $this->like = $data['like'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }      
        if (!empty($data['image'])) {
            $this->image = $data['image'];
        }        
        if (!empty($data['thread_desc'])) {
            $this->thread_desc = $data['thread_desc'];
        } else {
            $_SESSION['thread_req'] = "<font color='red'>Thread Description must required</font>";
        }
        if (!empty($data['comment'])) {
            $this->comment = $data['comment'];
        } else {
            $_SESSION['comment_req'] = "<font color='red'>Comment is empty!</font>";
        }        
        return $this;
    }

    public function index() {
        try {
            $query = "SELECT * FROM `thread` WHERE is_delete='0' ORDER BY `id` DESC";
            $q = $this->conn->query($query) or die('Unable to query');
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show() {
        $q = "SELECT * FROM `thread` WHERE unique_id=" . "'" . $this->id . "'";
        $result = $this->conn->query($q);
        $this->data = $result->fetch();

        return $this->data;
    }

    public function viewall() {
        $qry = "SELECT * FROM thread WHERE is_delete='0'";
        $stmt = $this->conn->query($qry);
        $stmt->execute();
        $table = $stmt->fetchAll();
        return $table;
    }

    public function view() {
        $qry = "SELECT * FROM thread WHERE username='$this->username'";
        $stmt = $this->conn->query($qry);
        $stmt->execute();
        $table = $stmt->fetchAll();
        return $table;
    }

    public function update() {
        try {
            $sql = "UPDATE `thread` SET `thread_desc`='$this->thread_desc',`like`='$this->like' WHERE unique_id = '$this->id'";
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // echo a message to say the UPDATE succeeded
            echo $stmt->rowCount() . " records UPDATED successfully";
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }
    
    
    public function comment() {
        try {
            $sql = "UPDATE `thread` SET `comment`='$this->comment' WHERE unique_id = '$this->id'";
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // echo a message to say the UPDATE succeeded
            echo $stmt->rowCount() . " Comment Posted Successfully";
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }    
    

    public function trash() {
        try {
            $sql = "UPDATE `thread` SET `is_delete`='1'  WHERE unique_id = '$this->id'";
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // echo a message to say the UPDATE succeeded
//            echo $stmt->rowCount() . " records UPDATED successfully";
            header("Location:index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

    public function trashted() {
        //$mydata = array();
        $q = "SELECT * FROM `thread` WHERE is_delete='1' ORDER BY `time` DESC";
        $result = $this->conn->query($q);
        $this->data = $result->fetchAll();
        return $this->data;
    }

    public function delete() {

        $sql = "DELETE FROM `thread` WHERE unique_id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $this->id));
        header('location:deleted.php');
        return true;
    }

    public function addthread() {
        try {
            $usrname = "'$this->username'";
            $qr = "SELECT * FROM thread WHERE thread_desc=" . $thread_desc;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user = $STH->fetch(PDO::FETCH_ASSOC);

            if (!empty($user)) {
                $_SESSION['Message'] = "Same Thread already exists";
                header('location:signup.php');
            } else {
                $verification_code = uniqid();
                $query = "INSERT INTO thread(id,unique_id,thread_desc,thread_creator,comment,is_approve,is_delete,image)
    VALUES(:id, :uid, :td, :tc, :cm, :ia, :isd,:image)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':uid' => uniqid(),
                    ':td' => $this->thread_desc,
                    ':tc' => $usrname,
                    ':cm' => 0,
                    ':ia' => 1,
                    ':isd' => 0,
                    ':image' => $this->image,
                ));
                $_SESSION['success_msg'] = "Successfully Thread Added.";


                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function restore() {
        try {
            $sql = "UPDATE `thread` SET `is_delete`='0'  WHERE unique_id = '$this->id'";
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // echo a message to say the UPDATE succeeded
//            echo $stmt->rowCount() . " records UPDATED successfully";
            header("Location:index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
    }

}
