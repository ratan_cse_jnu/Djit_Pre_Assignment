-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2017 at 09:07 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `abridge`
--

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

CREATE TABLE IF NOT EXISTS `thread` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(111) NOT NULL,
  `thread_desc` varchar(500) NOT NULL,
  `thread_creator` varchar(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  `comment` varchar(225) NOT NULL,
  `like` int(10) NOT NULL,
  `is_approve` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `image` varchar(225) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `thread`
--

INSERT INTO `thread` (`id`, `unique_id`, `thread_desc`, `thread_creator`, `user_id`, `comment`, `like`, `is_approve`, `is_delete`, `image`, `created`, `updated`, `deleted`, `time`) VALUES
(13, '58a9a76f25de8', 'phpBB is an Internet forum package written in the PHP scripting language. The name "phpBB" is an abbreviation of PHP Bulletin Board. Available under the GNU General Public License, phpBB is free and open source software.', 'Ratan', 1, 'Thanks for this information. pgpBB is a good website.', 0, 1, 0, '98e5aee377.jpeg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:14:38'),
(14, '58a9b18f2c508', 'FUDforum is a free and open source Internet forum software, originally produced by Advanced Internet Designs Inc., that is now maintained by the user community. The name "FUDforum" is an abbreviation of Fast Uncompromising Discussion forum. It is comparable to other forum software. FUDforum is customizable and has a large feature set relative to other forum packages.  FUDforum runs on a number of operating systems that are able to support the PHP programming language, including Unix, Linux and W', 'Ratan', 1, '0', 0, 1, 0, '9de3e465ce.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:14:43'),
(15, '58a9b3216476d', 'I have a question about joining two tables in mysql.  I have this data model:  timeline: structure: i.imgur.com/THmV5hp.jpg data: i.imgur.com/YaabHtf.jpg  i like to join 2 tables in differents rows. I try with left, inner and right join but its not possible. Results retrieve me in similar rows. I like a result like shown in this link:  This is a difficult question :P', 'Ratan', 1, '0', 0, 1, 0, '8c3fb6abec.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:14:50'),
(16, '58a9b38c5b461', 'Delete and Update Rows Using Inner Join in SQL Server. Inner join is used to select rows from multiple tables based on a matching column in one or more tables. It compares each row value of a table with each row value of another table to find equal values.', 'Ratan', 1, '0', 0, 1, 1, 'dbd694db42.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:14:54'),
(17, '58aaafbbd6338', 'Harry Potter is a British-American film series based on the Harry Potter novels by author J. K. Rowling. The series is distributed by Warner Bros. and consists of eight fantasy films, beginning with Harry Potter and the Philosopher''s Stone (2001) and culminating with Harry Potter and the Deathly Hallows â€“ Part 2 (2011).[2][3] A spin-off prequel series will consist of five films, starting with Fantastic Beasts and Where to Find Them (2016). The Fantastic Beasts films mark the beginning of a sha', 'Ratan', 1, '0', 0, 1, 0, '31433ea99e.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:14:58'),
(18, '58aab0c024c19', 'The Adventures of Tom Sawyer by Mark Twain is an 1876 novel about a young boy growing up along the Mississippi River. It is set in the fictional town of St. Petersburg, inspired by Hannibal, Missouri, where Twain lived.', 'Ratan', 1, '0', 0, 1, 0, '1e8d6c84cc.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:02'),
(19, '58aab136ea876', 'Albert Einstein (/ËˆaÉªnstaÉªn/;[4] German: [ËˆalbÉ›ÉÌ¯t ËˆaÉªnÊƒtaÉªn] ( listen); 14 March 1879 â€“ 18 April 1955) was a German-born theoretical physicist. He developed the general theory of relativity, one of the two pillars of modern physics (alongside quantum mechanics).[1][5]:274 Einstein''s work is also known for its influence on the philosophy of science.[6][7] Einstein is best known in popular culture for his massâ€“energy equivalence formula E = mc2 (which has been dubbed "the world''s m', 'Ratan', 1, '0', 0, 1, 0, '884a3fe57d.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:06'),
(20, '58ab329d7a794', 'How to get an Paypal Card Easily?', 'Ratan', 1, '0', 0, 1, 0, 'f6013419e5.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:09'),
(21, '58ab35cf9640e', 'International Mother Language Day (IMLD) is a worldwide annual observance held on 21 February to promote awareness of linguistic and cultural diversity and multilingualism. First announced by UNESCO on 17 November 1999, it was formally recognized by the United Nations General Assembly in its resolution establishing 2008 as the International Year of Languages.', 'Ratan', 1, '0', 0, 1, 0, 'd6090731e8.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:13'),
(22, '58ab37ea21740', 'A router[a] is a networking device that forwards data packets between computer networks. Routers perform the traffic directing functions on the Internet. A data packet is typically forwarded from one router to another router through the networks that constitute the internetwork until it reaches its destination node.', 'Ratan', 1, '0', 0, 1, 0, '63bb419847.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:17'),
(23, '58ab38ebe90d9', 'The Nintendo Switch is an upcoming video game console developed by Nintendo, and the company''s seventh major home console. Originally known in development as the NX, it was officially unveiled on October 20, 2016 and is scheduled for release worldwide on March 3, 2017.', 'Ratan', 1, '0', 0, 1, 0, 'a3469cd0ff.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:23'),
(24, '58ab3a0f6ec28', 'A plastic bag, polybag, or pouch is a type of container made of thin, flexible, plastic film, nonwoven fabric, or plastic textile. Plastic bags are used for containing and transporting goods such as foods, produce, powders, ice, magazines, chemicals, and waste. It is a common form of packaging.', 'Admin', 2, '0', 0, 1, 0, '629e179e7b.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:45'),
(25, '58ab3be8bc863', 'An electric fireplace is an electric heater that mimics a fireplace burning coal, wood, or natural gas. Electric fireplaces are often placed in conventional fireplaces, which can then no longer be used for conventional fires. They plug into the wall, and can run on a ', 'Admin', 2, '0', 1, 1, 0, 'a12e0ad086.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:39'),
(26, '58abb431cfaea', 'A computer virus is a type of malicious software program ("malware") that, when executed, replicates by reproducing itself (copying its own source code) or infecting other computer programs by modifying them.[1] Infecting computer programs can include as well, data files, or the "boot" sector of the hard drive.', 'Admin', 2, 'Antivirus Name List: FortiClient. ... AdAware Free Antivirus. ... Qihoo 360 Total Security. ... Comodo Antivirus. ... 6. Avira Free Antivirus. ... MalwareBytes. ... AVG Antivirus. ... Avast Free Antivirus.', 0, 1, 0, '21f5413a4d.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:15:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(225) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`, `last_login`) VALUES
(1, '58a81c66ec2bb', '', 'ratan', 'ratanroy198@yahoo.com', '12345', '', 1, 0, 0, '2017-02-18 04:05:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 05:28:36'),
(2, '58aa80175d80d', '', 'admin', 'admin@yahoo.com', 'admin', '', 1, 0, 0, '2017-02-20 11:35:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-02-21 06:03:26');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
