<?php

include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;

$obj = new Thread();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['thread_desc'])) {
        if (strlen($_POST['thread_desc']) > 5 && strlen($_POST['thread_desc']) < 1000) {

            $permited = array('jpg', 'jpeg', 'png', 'gif');

            $file_name = $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_temp = $_FILES['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_image = "upload/" . $unique_image;

            if (!empty($file_name)) {

                if ($file_size > 1048567) {
                    echo "<span class='error'>Image Size should be less then 1MB!</span>";
                } elseif (in_array($file_ext, $permited) === false) {
                    echo "<span class='error'>You can upload only:-" . implode(', ', $permited) . "</span>";
                } else {
                    move_uploaded_file($file_temp, $uploaded_image);
                    $_POST['image'] = $unique_image;
                    $obj->prepare($_POST)->addthread();
                    $as = $_SESSION['id'];
                    echo "<script> alert('Update succesfully')</script>";
                    echo "<script> window.location='index.php'</script>";
                }
            } else {
                $_SESSION['Message'] = "Image Required";
                header('location:create.php');
            }
        } else {
            $_SESSION['Message'] = "Thread Description Character Length must be 5 - 1000";
            header('location:create.php');
        }
    } else {
        $_SESSION['Message'] = "Thread description can't be empty";
        header('location:create.php');
    }
} else {
    $_SESSION['Message'] = "Opps something going wrong!";
    header('location:create.php');
}
