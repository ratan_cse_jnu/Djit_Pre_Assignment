<?php
include 'header.php';
include 'sidebar.php';
include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;

$obj = new Thread();
$data = $obj->index();
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>

<div class="content-wrapper">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Thread</li>
            </ul>
        </div>
    </div>  

    <div class="col-lg-12 centered " >
        <div class="col-lg-4 centered " >

            <div class="">                
                <table border="1" width="1000">
                    <tr>
                        <th width="50"><center>SL</center></th>
                    <th width="100"><center>Creator</center></th>
                    <th width="350"><center>Thread</center></th>
                    <th width="200"><center>Poster</center></th>                    
                    <th width="100"><center>Like by You</center></th>
                    <th colspan="3" width="200"><center>Action</center></th>
                    </tr>
                    <?php
                    $serial = 1;
                    if (isset($data) && !empty($data)) {
                        foreach ($data as $value) {
                            ?>
                            <tr>
                                <td><center> <?php echo $serial++ ?></center></td>
                            <td><center> <?php echo $value['thread_creator'] ?></center></td>
                            <td><center> <?php echo $value['thread_desc'] ?><br/><a href="comment.php?id=<?php echo $value['unique_id'] ?>">Comment</a></center><br/>
                            <b>Comment:</b><?php echo $value['comment'] ?></td>
                            <td><center> <img src="<?php echo 'upload/' . $value['image']; ?>" alt='Invalid File' class='thumbnail' height='200px;' width='200px;' /> </center></td>                    
                            <td><center> <?php if ($value['like'] == 1) { ?>
                                    <input type="checkbox" name="like" id="like" value="yes" <?php echo ($value['like'] == 1 ? 'checked' : ''); ?>>
                                    <?php
                                } else {
                                    ?>
                                    <input type="checkbox" name="like" id="like" value="yes" <?php echo ($value['like'] == 0 ? 'unchecked' : ''); ?>>  
                                <?php }
                                ?></center></td>
                            <td><center><a href="show.php?id=<?php echo $value['unique_id'] ?>">View</a></center></td>
                            <td><center><a href="edit.php?id=<?php echo $value['unique_id'] ?>">Update</a></center></td>
                            <td><center><a href="trash.php?id=<?php echo $value['unique_id'] ?>">Delete</a></center></td>
                            </tr>

                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="3">
                                No available data. <a href="create.php">Add Data</a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>

                <div class="container-fluid">
                    <div id="members-online">
                        <br/>
                        <center><a href="deleted.php">View all Deleted Threads</a></center>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
</div>
</div>

<script>
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button";//again because google chrome don't insert first hash into history
    window.onhashchange=function(){window.location.hash="no-back-button";}
</script>
</body>
</html>
