<?php
include 'header.php';
include 'sidebar.php';
include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;
?>
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="">Deleted Thread</a></li>
        </ul>
    </div>
</div>

<center><a href="create.php">Add New Thread</a> |
    <a href="index.php">See All Threads</a></center>
<?php
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$Objects = new Thread();

$data = $Objects->trashted();
?>
<html>
    <head>        
        <title> Index | Data </title>
    </head>
    <body>
    <center>         
        <table border="1" width="1000">
            <tr>
                <th width="50"><center>SL</center></th>
                <th width="100"><center>Creator</center></th>
                <th width="350"><center>Thread</center></th>
                <th width="200"><center>Poster</center></th>                    
                <th width="100"><center>Like by You</center></th>
                <th colspan="4" width="200"><center>Action</center></th>
            </tr>
            <?php
            $serial = 1;
            if (isset($data) && !empty($data)) {
                foreach ($data as $value) {
                    ?>

                    <tr>
                    <td><center> <?php echo $serial++ ?></center></td>
                    <td><center> <?php echo $value['thread_creator'] ?></center></td>        
                    <td><center> <?php echo $value['thread_desc'] ?><br/><a href="comment.php?id=<?php echo $value['unique_id'] ?>">Comment</a></center><br/>
                    <b>Comment:</b><?php echo $value['comment'] ?></td>
                    <td><center> <img src="<?php echo 'upload/' . $value['image']; ?>" alt='Invalid File' class='thumbnail' height='200px;' width='200px;' /> </center></td>         
                    <td><center> <?php if ($value['like'] == 1) { ?>
                            <input type="checkbox" name="like" id="like" value="yes" <?php echo ($value['like'] == 1 ? 'checked' : ''); ?>>
                            <?php
                        } else {
                            ?>
                            <input type="checkbox" name="like" id="like" value="yes" <?php echo ($value['like'] == 0 ? 'unchecked' : ''); ?>>  
                        <?php } ?></center></td>

                    <td width="30"><a href="show.php?id=<?php echo $value['unique_id'] ?>">View</a></td>
                    <td width="30"><a href="edit.php?id=<?php echo $value['unique_id'] ?>">Update</a></td>
                    <td width="160"><a href="delete.php?id=<?php echo $value['unique_id'] ?>"><center>Delete Permanently</center></a></td>
                    <td width="30"><a href="restore.php?id=<?php echo $value['unique_id'] ?>">Restore</a></td>                  

                    </tr>

                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3">
                        No available Thread. <a href="create.php">Add Thread</a>
                    </td>
                </tr>
            <?php } ?>

        </table>
    </center>
</body>
</html>
