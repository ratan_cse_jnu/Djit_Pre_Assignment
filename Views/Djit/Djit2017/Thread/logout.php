<?php

include_once"../../../../vendor/autoload.php";

use Apps\Djit\Djit2017\Users\Users;

$obj = new Users();

if ($_SERVER['REQUEST_METHOD'] == "GET" && $_GET['id'] = $_SESSION['user']['id']) {

    if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {

        unset($_SESSION['user']);
        $_SESSION['Message'] = "<center><h3><font color='red'>Logged Out Successfully </font></h3></center>";
        header('location:../Login/login.php');
    }
} else {
    header('location:logout.php');
}
?>
