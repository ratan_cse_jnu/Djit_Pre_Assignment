<?php
include 'header.php';
include 'sidebar.php';
include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;

$object = new Thread();

$object->prepare($_GET);
$data = $object->show();
if (isset($data) && !empty($data)) {
    ?>

    <?php
    if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
    ?>

    <html>
        <head>
            <title>Comment - ABridge Corp.</title>
        </head>
        <body>
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">Input Comment</li>
                    </ul>
                </div>
            </div>            
        <center>

            <form action="comment2.php" method ="POST">
                Input your Comment:
                <input type="text" style="width: 500px; height: 100px; padding: 2px; border: 1px solid black" name="comment" value="<?php echo $data['comment'] ?>" id="comment_mod" >
                <br/>
                <input type="hidden" name="id"  value="<?php echo $_GET['id'] ?>"><br>
                <br/>
                <input type="submit" value="Submit Comment" onclick="myFunction()">

            </form>              
        </center>

    </body>
    </html>

    <script>
        function myFunction() {

            var input_value = document.getElementById('comment_mod').value;
            console.log(input_value);
            if (input_value == "") {
                alert("Please enter a valid Comment");
            } else {
                alert("You have put a Comment");

            }
        }
    </script>
    <?php
} else {
    $_SESSION['Message'] = "<br/><center>Not found, you are trying to update another information.</center>";
    header('location:error.php');
}
?>