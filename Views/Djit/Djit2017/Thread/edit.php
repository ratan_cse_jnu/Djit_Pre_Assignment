<?php
include 'header.php';
include 'sidebar.php';
include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;

$object = new Thread();

$object->prepare($_GET);
$data = $object->show();
if (isset($data) && !empty($data)) {
    ?>

    <?php
    if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
    ?>

    <html>
        <head>
            <title>Update - ABridge Corp.</title>
        </head>
        <body>
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">Update Thread</li>
                    </ul>
                </div>
            </div>            
        <center>

            <form action="update.php" method ="POST">

                Update Your Thread:
                <input type="text" style="width: 500px; height: 100px; padding: 2px; border: 1px solid black" name="thread_desc" value="<?php echo $data['thread_desc'] ?>" id="thread_mod" >
                <br/><br/>
                Like This:
                <?php if ($data['like'] == 1) { ?>
                    <input type="checkbox" name="like" id="like_mod" value="1" <?php echo ($data['like'] == 1 ? 'checked' : ''); ?>>
                    <?php
                } else {
                    ?>
                    <input type="checkbox" name="like" id="like_mod" value="1" <?php echo ($data['like'] == 0 ? 'unchecked' : ''); ?>>  
                <?php }
                ?>
                <input type="hidden" name="id"  value="<?php echo $_GET['id'] ?>"><br>
                <br/>
                <input type="submit" value="Update" onclick="myFunction()">
            </form>              
        </center>
    </body>
    </html>


    <script>
        function myFunction() {

            var input_value = document.getElementById('thread_mod').value;
            console.log(input_value);
            if (input_value == "") {
                alert("Please enter a valid Thread Description");
            } else {
                alert("You have updated Thread Description");

            }
                    
                    
            var input_value = document.getElementById('like_mod').value;
            console.log(input_value);
            if (input_value == "") {
                alert("Check the box if you Like");
            } else {
                alert("You Like this Thread");

            }
        }
    </script>
    <?php
} else {
    $_SESSION['Message'] = "<br/><center>Not found, you are trying to update another information.</center>";
    header('location:error.php');
}
?>