<?php
include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Users\Users;

$obj = new Users();
$_POST['uname'] = $_SESSION['uname'];

if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {
    
} else {
    session_destroy();
    $_SESSION['Message'] = "Login for continue";
    header('location:login.php');
}

$obj->prepare($_POST);
$data = $obj->view();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dashboard</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>
        <script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
        <script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>        

        <style type="text/css">

            .paging-nav {

                position: absolute;
                /* float: right; */
                margin-top: 20px;
                margin-left: 30%;
            }

            .paging-nav a {
                margin: auto 1px;
                text-decoration: none;
                display: inline-block;
                padding: 1px 7px;
                background: #91b9e6;
                color: white;
                border-radius: 3px;
            }

            .paging-nav .selected-page {
                background: #187ed5;
                font-weight: bold;
            }


        </style>

    </head>
    <body>
        <?php foreach ($data as $a) { ?>

            <div class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand" href=""></a>
                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

                        <li class="dropdown dropdown-user dropdown-menu-right">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img src="upload/profile.jpg" alt="">

                                <span><?php echo ucfirst($a['username']); ?></span>

                                <i class="caret"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="logout.php?id=<?php echo $a['unique_id']; ?>"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        <?php } ?>
