<?php
include '../Login/header.php';
include '../Login/sidebar.php';
?>
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="">Thread</a></li>
        </ul>
    </div>
</div>
<?php
include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Thread\Thread;

$object = new Thread();

$object->prepare($_GET);
$data = $object->show();
if (isset($data) && !empty($data)) {
    ?>
    <body>
    <center>
        <a href="index.php">Back to list</a>
        <table border="1" width="950">
            <tr>
                <th width="100"><center>ID</center></th>
            <th width="350"><center>Thread</center></th>
            <th width="300"><center>Poster</center></th>                  
            <th width="100"><center>Like by You</center></th>
            </tr>
            <tr>
                <td><?php echo $data['id'] ?></td>
                <td><?php echo $data['thread_desc'] ?></td>
                <td><center> <img src="<?php echo 'upload/' . $data['image']; ?>" alt='Invalid File' class='thumbnail' height='200px;' width='200px;' /> </center></td>                  
            <td><center> <?php if ($data['like'] == 1) { ?>
                    <input type="checkbox" name="like" id="like" value="yes" <?php echo ($data['like'] == 1 ? 'checked' : ''); ?>>
                    <?php
                } else {
                    ?>
                    <input type="checkbox" name="like" id="like" value="yes" <?php echo ($data['like'] == 0 ? 'unchecked' : ''); ?>>  
                <?php } ?></center></td>
            </tr>
        </table>
    </center>
    </body>
    <?php
} else {
    $_SESSION['Message'] = "<br/><center>Not found, you are trying to show another page. </center>" . "<a href= 'Create.php'></a>";
    header('location:error.php');
}?>