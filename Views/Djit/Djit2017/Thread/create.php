<?php
include 'header.php';
include 'sidebar.php';

include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Users\Users;

$obj = new Users();
$obj->prepare($_POST);
$data = $obj->view();
?>
<?php foreach ($data as $a) { ?>
    <div class="content-wrapper">

        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li><a href="">Add Thread</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <style type='text/css'>
                    .bgtitle {
                        color:#000000; background-color:#C0C0C0;
                        font-family:arial, verdana, sans-serif; font-size:12pt;                        
                        font-size:12px;
                        padding:10px;
                        margin-left: 0px;
                        width:600px;
                        line-height:1.8;}
                    </style>
                    <div class="bgtitle">

                    <form action="addthread.php" method="post" enctype="multipart/form-data">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Add Thread</h5>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <label><font color="red">*</font>Thread Description:</label>
                                    <input type="text" class="form-control" placeholder="Thread Description [5-1000]" name="thread_desc" required>
                                </div>  
                                <div class="form-group">
                                    <label>Published by:</label>
                                    <input type="text" class="form-control" value="<?php echo ucfirst($a['username']); ?>" name="thread_creator"  readonly>
                                </div> 
                                <div class="form-group">
                                    <label for="sel1">Subject:</label>
                                    <select class="form-control" id="sel1" name="is_approve" required>
                                        <option value="1">Technology</option>
                                        <option value="0">Non Technology</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label><font color="red">*</font>Upload Poster: [Max: 1MB]</label>
                                    <input type="file" class="form-control" name="image" required>
                                </div>

                                <div class="text-right">
                                    <input type="submit" class="btn btn-primary" name="submit" value="Add Thread">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </body>
    </html>
<?php } ?>