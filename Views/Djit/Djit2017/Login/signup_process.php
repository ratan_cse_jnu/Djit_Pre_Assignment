<?php

include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Users\Users;

$obj = new Users();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['uname'])) {
        if (strlen($_POST['uname']) > 3 && strlen($_POST['uname']) < 10) {
            if (!empty($_POST['pw1'])) {
                if ($_POST['pw1'] == $_POST['pw2']) {
                    if (strlen($_POST['pw1']) > 4 && strlen($_POST['pw1']) < 10) {
                        if (!empty($_POST['email'])) {
                            $obj->prepare($_POST)->signup();
                        } else {
                            $_SESSION['Message'] = "Email Required";
                            header('location:signup.php');
                        }
                    } else {
                        $_SESSION['Message'] = "<h4><font color='red'><center>Password Length Should be 5-10 Characters</center><h4></font>";
                        header('location:signup.php');
                    }
                } else {
                    $_SESSION['Message'] = "<h4><font color='red'><center>Password does not match</center><h4></font>";
                    header('location:signup.php');
                }
            } else {
                $_SESSION['Message'] = "<h4><font color='red'><center>Password can't be empty</center><h4></font>";
                header('location:signup.php');
            }
        } else {
            $_SESSION['Message'] = "<h4><font color='red'><center>Username Length must be 3 - 10 Characters</center><h4></font>";
            header('location:signup.php');
        }
    } else {
        $_SESSION['Message'] = "<h4><font color='red'><center>User name can't be empty</center><h4></font>";
        header('location:signup.php');
    }
} else {
    $_SESSION['Message'] = "<h4><font color='red'><center>Opps something going wrong!</center><h4></font>";
    header('location:signup.php');
}
