<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Login - ABridge Corp.</title>
    </head>
    <style type='text/css'>
        .bgtitle {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 170px;
            width:970px;
            line-height:1.8;}

        fieldset {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 550px;
            width:250px;
            line-height:1.8;}        

        .bodyarea {
            color:#000000; background-color:#ffffff;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            height:400px;
            line-height:1.8;}        

        .footer {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 170px;
            width:970px;
            line-height:1.8;}       
        </style>
        <div class="bodyarea">
        <body class="bg-slate-800">
        <div class="bgtitle"><center><h2><font color="red">ABridge Corp.</font></h2></center>
            <center><h5><a href="/abridge/index.php">Home</a></h5></center></div>  
        <form  method="post" action="login_process.php">
            <div class="">
                <div class="">
                    <?php if (isset($_SESSION['Message'])) { ?>
                        <?php
                        echo $_SESSION['Message'];
                        unset($_SESSION['Message']);
                        ?>
                    <?php } ?>
                    <?php if (isset($_SESSION['success_msg1'])) { ?>
                        <?php
                        echo $_SESSION['success_msg1'];
                        unset($_SESSION['success_msg1']);
                        ?>
                    <?php } ?>
                    <center><h3>Login to your account </h3></center>
                </div>

                <fieldset>
                    <div class="">
                        <center><input type="text" class="" placeholder="Username" name="uname"></center>
                    </div>
                    <br/>
                    <div class="">
                        <center><input type="password" class="" placeholder="Password" name="pw1"></center>
                    </div>							

                    <div class="">
                        <center><input type="submit" name="submit" id="submit" value="Login" class="" ></center>
                    </div>

                    <div class=""><span><center>Don't have an account?</center></span></div>
                    <center><a href="signup.php" class="">Register</a></center>
                </fieldset>
            </div>
        </form>
        </div>
        <br/>
        <div class="footer"><center><h2><font color="black">&copy; Copyright 2017 ABridge Corp.</font></h2></center></div>   
    </body>
</html>
