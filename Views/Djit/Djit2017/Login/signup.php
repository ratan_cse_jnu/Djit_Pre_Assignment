<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sign Up - ABridge Corp.</title>
    </head>
    <style type='text/css'>
        .bgtitle {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 170px;
            width:970px;
            line-height:1.8;}

        fieldset {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 535px;
            width:250px;
            line-height:1.8;}       

        .bodyarea {
            color:#000000; background-color:#ffffff;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            height:500px;
            line-height:1.8;}        

        .footer {
            color:#000000; background-color:#C0C0C0;
            font-family:arial, verdana, sans-serif; font-size:12pt;                        
            font-size:12px;
            padding:10px;
            margin-left: 170px;
            width:970px;
            line-height:1.8;}       
        </style>
        <div class="bodyarea">
        <body class="bg-slate-800">
            <div class="bgtitle"><center><h2><font color="red">ABridge Corp.</font></h2></center>
                <center><h4><a href="/abridge/index.php">Home</a></h4></center></div> 
            <form  method="POST" action="signup_process.php" class="form" id="form">
                <div class="panel panel-body login-form">
                    <div class="text-center">
                        <?php if (isset($_SESSION['Message'])) { ?><span class="">

                                <?php
                                echo $_SESSION['Message'];
                                unset($_SESSION['Message']);
                                ?>
                            </span><?php } ?>
                        <?php if (isset($_SESSION['success_msg'])) { ?><span class="">
                                <?php
                                echo $_SESSION['success_msg'];
                                unset($_SESSION['success_msg']);
                                ?>
                            </span><?php } ?>
                        <center><h2>Create account</h2></center>
                    </div>                    
                    <fieldset>        
                        <center><small class=""><font color="red">All fields are required</font></small></center>
                        <div>
                            <center>Your information</center>
                        </div>
                        <div>
                            <center><input type="text" class="" placeholder="Username [3-10]" name="uname" id="uname" required></center>
                        </div>
                        <br/>
                        <div>
                            <center><input type="email" class="" placeholder="Email" name="email" id="email" required></center>
                        </div>
                        <br/>
                        <div>
                            <center><input type="password" class="" placeholder="Create password [5-10]" name="pw1" id="pw1" required></center>
                        </div>
                        <br/>
                        <div>
                            <center><input type="password" class="" placeholder="Repeat password" name="pw2" id="pw2" required></center>
                        </div>
                        <br/>
                        <center><input type="submit" value="Create an account" class="" name="submit" onClick="validatePassword();"></center>
                        <div>
                            <center><span>Already have an account?</span></center>
                        </div>
                        <br/>
                        <center><a href="login.php" class="">Login</a></center>
                    </fieldset>
                </div>
            </form>
    </div>
    <br/>
    <div class="footer"><center><h2><font color="black">&copy; Copyright 2017 ABridge Corp.</font></h2></center></div>         
</body>
</html>
