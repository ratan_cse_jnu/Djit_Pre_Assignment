/**
 * Created by Debashish on 8/16/2016.
 */
$(document).ready(function() {

    // process the form
    $('form').submit(function(event) {
        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text
        $("#errdiv").html(" ");
        $("#successmsg").html("");
        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'courseId'          : $('select[name=courseId]').val(),
            'leadTrainerId'     : $('select[name=leadTrainerId]').val(),
            'assistantTrainerId': $('select[name=assistantTrainerId]').val(),
            'labAssistantId'    : $('select[name=labAssistantId]').val(),
            'labNumber'         : $('select[name=labNumber]').val(),
            'day'               : $('select[name=day]').val(),
            'startDate'         : $('input[name=startDate]').val(),
            'endDate'           : $('input[name=endDate]').val(),
            'startTime'         : $('input[name=startTime]').val(),
            'endTime'           : $('input[name=endTime]').val(),
            'batch'             : $('input[name=batch]').val(),
            'is_running'        : $('select[name=is_running]').val()
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'StoreCourse.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
                // here we will handle errors and validation messages
                if ( ! data.success) {
                    $.each(data.errors,function (i,v) {
                        $("#errdiv").append("<div style='color: red; margin-left: 10px; background-color: wheat;'><p>" + v + "</p></div>");
                    })
                    //$("#errdiv").html(data.errors[0]);
                    // handle errors for name ---------------
                    // if (data.errors.name) {
                    //     $('#name-group').addClass('has-error'); // add the error class to show red input
                    //     $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                    // }
                    //
                    // // handle errors for email ---------------
                    // if (data.errors.email) {
                    //     $('#email-group').addClass('has-error'); // add the error class to show red input
                    //     $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                    // }
                    //
                    // // handle errors for superhero alias ---------------
                    // if (data.errors.superheroAlias) {
                    //     $('#superhero-group').addClass('has-error'); // add the error class to show red input
                    //     $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                    // }

                } else {
                    //$("#successmsg").html('<h5 class="panel-title" id="" style="background-color: #7EFC7E;padding: 5px;">'+data.message+'</h5>');
                    // ALL GOOD! just show the success message!
                    //$('form').append('<div class="alert alert-success">' + data.message + '</div>');
                    //window.location= "index.php";
                    // usually after form submission, you'll want to redirect
                    // window.location = '/thank-you'; // redirect a user to another page
                    //alert('success'); // for now we'll just alert the user
                    //$("#CreateCourse")[0].reset();
                    window.location.href  = "CreateCourse.php";

                }
            })
        .fail(function(data) {

            // show any errors
            // best to remove for production
            console.log(data);
            console.log(formData);
        });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});