<?php

include_once '../../../../vendor/autoload.php';

use Apps\Djit\Djit2017\Users\Users;

$obj = new Users();

$_SESSION['uname'] = $_POST['uname'];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['uname'])) {
        if (!empty($_POST['pw1'])) {
            $obj->prepare($_POST)->login();
        } else {
            $_SESSION['Message'] = "<h4><font color='red'><center>Enter password</center></font></h4>";
            header('location:signup.php');
        }
    } else {
        $_SESSION['Message'] = "<h4><font color='red'><center>Enter username</center></font></h4>";
        header('location:signup.php');
    }
} else {
    $_SESSION['Message'] = "<h4><font color='red'><center>Opps something going wrong!</center></font></h4>";
    header('location:signup.php');
}
