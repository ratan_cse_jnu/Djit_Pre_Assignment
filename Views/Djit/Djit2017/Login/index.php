<?php
include 'header.php';
include 'sidebar.php';
?>

<div class="content-wrapper">

    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>  

    <div class="col-lg-12 centered " >
        <div class="col-lg-4 centered " >

            <div class="panel bg-teal-400">
                <div class="panel-body">
                    <div class="heading-elements">
                    </div>
                    <?php
                    $last = $obj->view();
                    foreach ($last as $l) {
                        ?>
                        <h3 class="no-margin">Welcome <?php echo $l['username'] ?></h3>

                        <div class="text-muted text-size-small">Last login <?php echo $l['last_login'] ?> </div>

                    <?php } $obj->Last_login(); ?>
                </div>

                <div class="container-fluid">
                    <div id="members-online"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 centered ">

            <div class="panel bg-blue-400 text-center">
                <div class="panel-body">


                    <h5 class="no-margin">Total Thread Posts by You</h5>

                    <h3><?php $obj->noOfPosts() ?></h3>
                </div>

                <div id="today-revenue"></div>
            </div>

        </div>        

    </div> 
</div>
</div>
</div>

<script>
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button";//again because google chrome don't insert first hash into history
    window.onhashchange=function(){window.location.hash="no-back-button";}
</script>
</body>
</html>
